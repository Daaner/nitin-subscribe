$(document).ready(function() {

  var $login = $('#login');
  var $reffer = $('#reffer');
  var $reset = $('#reset-password');

  var $logblock = $('.lb');
  var $regblock = $('.rb');

  var $showpwd = $('.showpwd');
  $showpwd.find('.pwd-btn').click(function(){
    $(this).toggleClass('show');
    if($(this).hasClass('show')){
      $(this).find('i').addClass('fa-eye-slash');
      $(this).find('i').removeClass('fa-eye');
      $(this).parent().find(':password').attr('type', 'text');
    } else {
      $(this).find('i').addClass('fa-eye');
      $(this).find('i').removeClass('fa-eye-slash');
      $(this).parent().find(':text').attr('type', 'password');
    }
  });



  function formRegister() {
    $login.find(':submit').text('Join');
    $login.find('.h3').text('Create Account');
    $login.find('.fb-button span').text('Join with Facebook');
    $login.find($regblock).slideDown();
    $login.find($logblock).slideUp();
    $login.find('.repassword input').prop('required',true);
    $login.find('.repassword input').attr('disabled',false);
  };

  function formLogin() {
    $login.find(':submit').text('Login');
    $login.find('.h3').text('Login');
    $login.find('.fb-button span').text('Login with Facebook');
    $login.find($regblock).slideUp();
    $login.find($logblock).slideDown();
    $login.find('.repassword input').prop('required',false);
    $login.find('.repassword input').attr('disabled',true);
  };

  if(1){
    formLogin();
  }


  $login.find('.radio input').change(function() {
    if($('#newCustomers').is(':checked')) {
      formRegister();
    } else {
      formLogin();
    }
  });



});
