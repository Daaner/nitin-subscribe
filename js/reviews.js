$(document).ready(function() {
  var $block = $('.tabs-menu');
  var $control = $('.tabs-menu .controls');
  var $tabmenu = $('.tabs-menu .menu');
  var $revblock = $('.all-reviews');


  //each for button
  var hideButton = 88;

  $revblock.find('.review-item-block').each(function(){
    var $block = $(this).find('.review-text');
    var oldH = $block.height();
    if(oldH < hideButton){
      $(this).find('.show-text-r').attr('style', 'visibility:hidden;');
    }

    $block.attr('style', 'max-height:' + parseInt(oldH) + '.9px; transition:0s;');
    if(oldH == $block.height()){
      $(this).find('.change-show-text-r').attr('style', 'visibility:hidden;');
    } else {
      $(this).find('.change-show-text-r').removeAttr('style');
    }
    $block.removeAttr('style');
  });



  var blockwidth = $block.width();
  var tabswidth = 0;
  $tabmenu.find('li').each(function(){
    tabswidth += $(this).width();
  });

  //screen resize function
  $(window).resize(function() {
    blockwidth = $block.width();
    if(tabswidth <= blockwidth){
      $block.addClass('no-controls');
    } else {
      $block.removeClass('no-controls');
    }

    //each for button
    $revblock.find('.review-item-block').each(function(){
      $(this).removeClass('more');
      $(this).find('.change-show-text-r').attr('style', 'visibility:hidden;');
      var $block = $(this).find('.review-text')
      oldH = $block.height();
      $block.attr('style', 'max-height:' + parseInt(oldH) + '.9px; transition:0s;');
      if(oldH < $block.height()){
        $(this).find('.change-show-text-r').removeAttr('style');
      }
      $block.removeAttr('style');
    });
  });

  if(tabswidth <= blockwidth){
    $block.addClass('no-controls');
  } else {
    $block.removeClass('no-controls');
  }

  $tabmenu.bind('mousewheel', function(event) {
    this.scrollLeft += event.originalEvent.deltaY;
  });

  $control.find('.next').on('click', function(e) {
    $tabmenu.scrollLeft($tabmenu.scrollLeft() + 20 );
    e.preventDefault();
  });
  $control.find('.previous').on('click', function(e) {
    $tabmenu.scrollLeft($tabmenu.scrollLeft() - 20 );
    e.preventDefault();
  });

  var curDown = false,
      afterPosition = 0,
      delta = 0,
      curXPos = 0;
  $tabmenu.mousemove(function(m){
    if(curDown === true){
      $tabmenu.scrollLeft($(window).scrollLeft() + (curXPos - m.pageX));
    }
  });

  $tabmenu.mousedown(function(m){
    curDown = true;
    curXPos = m.pageX + $tabmenu.scrollLeft();
    m.preventDefault();
  });

  $(window).mouseup(function(){
    curDown = false;
  });

  //Showmore case1
  $('.change-show-text-r').click(function(e) {
    var $more = $(this).parents('.review-item-block');
    $more.toggleClass('more');
    e.preventDefault();
  });

  //Showmore case2
  $('.show-text-r').click(function(e) {
    $(this).parents('.review-item-block').addClass('more');
    e.preventDefault();
  });
  $('.hide-text-r').click(function(e) {
    $(this).parents('.review-item-block').removeClass('more');
    e.preventDefault();
  });


  // var footer = $('#rev_page .button-send');
  // if (footer){
  //   $('footer').addClass('fixbutton');
  // }

});
