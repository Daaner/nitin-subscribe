$(document).ready(function() {

  var $bulletsprogress = $('.orbit-progress .bullets-progress');
  var $bulletsfill = $('.bullet-filled');
  var timeprogress = 5000;
  var timesec = 1;
  var autoplay = true;

  $($bulletsfill).find('button').each(function() {
    $(this).find(".fill").clone().addClass("fill-copy").appendTo(this);
  });

  function clearProgress() {
    timesec = 1;
    $bulletsfill.find('button:not(.is-active)').find('.fill-copy').css({
      'width': '0',
      'transition': 'none'
    });

    $bulletsprogress.find('button:not(.is-active)').find('progress').val(0);
  }

  $('.orbit-progress').on('slidechange.zf.orbit', function() {
    clearProgress();
    if (!autoplay) {
      $bulletsprogress.find('.is-active').find('progress').val(100);
    }
  });

  var IntervalProgress = setInterval(function() {

    $bulletsfill.find('.is-active').find('.fill-copy').css({
      'width': '100%',
      'transition': timeprogress / 1000 + 's linear'
    });
    $bulletsprogress.find('.is-active').find('progress').val(timesec);


    timesec++;
    if (timesec == 100) {
      timesec = 1;
      clearProgress();
      $('.orbit-progress').foundation('changeSlide', true);
    }
  }, timeprogress / 100);

  $('.orbit-progress .dots-progress button').on('click', function() {
    clearProgress();
    clearInterval(IntervalProgress);
  });

  $bulletsprogress.find('button').on('click', function() {
    $('.orbit-progress').siblings('.slider-pause').click();
    autoplay = false;
    $(this).find('.fill-copy').css({
      "width": '100%',
      'transition': 'none'
    });
    $(this).find('progress').val(100);

    clearInterval(IntervalProgress);
  });

});
