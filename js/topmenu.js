$(document).ready(function() {
  widthAll = $(document).width();

  $(window).resize(function() {
    width = $(window).width();
    if(width >= 1024){
      $('.topmenu').removeClass('open');
      $('.topmenu .dropdown').removeClass('sub-open');
      $('.topmenu .dropdown').find('.submenu').slideUp();
    }
  });

  $('.topmenu .dropdown .submenu a img').closest('.menu').addClass('have-image');

  $('.topmenu .open-menu, .mobile-close-button').on('click', function() {
    if ($('.topmenu').hasClass('open')) {
      $('.topmenu').removeClass('open');
      $('.topmenu .dropdown').removeClass('sub-open');
      $('.topmenu .dropdown').find('.submenu').slideUp('slow');

      $('html, body').css({
        overflow: 'auto',
        height: 'auto'
      });

      $(this).find('.main').slideDown();
    } else {
      $('.topmenu').addClass('open');

      $('html, body').css({
        overflow: 'hidden',
        height: '100%'
      });

      $(this).find('.main').slideUp();
    }
  });

  $('.topmenu .dropdown').on('click', function(e) {
    if(widthAll < 1024){
      $(this).toggleClass('sub-open');
      if ($(this).hasClass('sub-open')) {
        $('.topmenu .dropdown').not(this).removeClass('sub-open');
        $('.topmenu .dropdown').not(this).find('.submenu').slideUp('slow');
        $(this).find('.submenu').slideDown('slow');
      } else {
        $(this).find('.submenu').slideUp('slow');
      }
    }
    // e.preventDefault();
  });

});
