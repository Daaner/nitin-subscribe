//localStorage or sessionStorage
var subScribe = sessionStorage.getItem("subscribe");
var subScribeTop = sessionStorage.getItem("subscribe-top");
var subScribeAll = localStorage.getItem("subscribe");


$(document).ready(function() {
  function subShow() {
    $('.subscribe_hide').hide();
    $('.subscribe_b').fadeIn();
    sessionStorage.removeItem("subscribe");
  }

  function subHide() {
    $('.subscribe_hide').fadeIn(1500);
    $('.subscribe_b').hide();
    sessionStorage.setItem("subscribe", true);
  }

  if (!subScribe) {
    subShow();
  } else {
    subHide();
  }

  //--- MAN --- after subscribe complite add in localStorage key
  //localStorage.setItem("subscribe", true);
  if (!subScribeTop & !subScribeAll) {
    $('.subscribe_top').slideDown(700);
  }

  $('.subscribe-close-button').click(function(e) {
    subHide();
    e.preventDefault();
  });

  $('.subscribe-close-button-top').click(function(e) {
    $('.subscribe_top').slideUp(700);
    sessionStorage.setItem("subscribe-top", true);
    subHide();
    e.preventDefault();
  });

  $('.subscribe-show-button').click(function(e) {
    subShow();
    e.preventDefault();
  });

  //carusel
  $('.owl-carousel').owlCarousel({
    loop: true,
    nav: true,
    rewind: true,
    autoplay: true,
    stagePadding: 50,
    autoplayHoverPause: true,
    lazyLoad: true,
    dots: false,
    navText: ['<i class="fa fa-caret-left"></i>', '<i class="fa fa-caret-right"></i>'],
    autoplayTimeout: 3000,
    smartSpeed: 1000,
    margin: 10,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        stagePadding: 5
      },
      480: {
        items: 2
      },
      1000: {
        items: 3
      },
      1600: {
        items: 5
      }
    }
  });

  //zoom
  var $panzoom = $('.reveal').find('.panzoom').first();
  var $pancontrol = $('.reveal .control').first();

  function zoomIn() {
    $pancontrol.find('.zoom-in').addClass('disabled');
    $pancontrol.find('.zoom-out').removeClass('disabled');
    $pancontrol.find('.reset').removeClass('disabled');
  }

  function zoomOut() {
    $panzoom.panzoom("setMatrix", [1, 0, 0, 1, 0, -25]);
    $pancontrol.find('.zoom-out').addClass('disabled');
    $pancontrol.find('.reset').addClass('disabled');
    $pancontrol.find('.zoom-in').removeClass('disabled');
  }

  function zoomAll() {
    $pancontrol.find('.zoom-in').removeClass('disabled');
    $pancontrol.find('.zoom-out').removeClass('disabled');
    $pancontrol.find('.reset').removeClass('disabled');
  }

  // contain: 'invert',
  $panzoom.panzoom({
    minScale: 1,
    maxScale: 2.8,
    rangeStep: 1,
    easing: "ease-in-out",
    $zoomIn: $(".zoom-in"),
    $zoomOut: $(".zoom-out"),
    $reset: $(".reset")
  });

  $panzoom.on('panzoomzoom', function(e, scale, opts, changed) {
    var scaled = $panzoom.panzoom("getMatrix");
    var scale_max = $panzoom.panzoom('option', 'maxScale');
    var scale_min = $panzoom.panzoom('option', 'minScale');
    if (changed) {
      if (opts > 1) {
        if (scaled[0] == scale_max) {
          zoomIn();
        } else {
          zoomAll()
        }
      }
      if (opts < 1) {
        if (scaled[0] == scale_min) {
          zoomOut();
        } else {
          zoomAll()
        }
      }
    }
  });

  $panzoom.on('panzoomreset', function(e, change) {
    if (change) {
      zoomOut();
    }
  });

  // next/prev button
  // var links = $('.carusel .owl-item:not(.cloned) a.zoom img').map(function(el){
  //   return $(this).attr('data-image');
  // }).get().join("\n");
  // console.log(links);

  $('.owl-carousel a.zoom').on('click', function(e) {
    var $image = $(this).children('img').attr('data-image');
    if($image){$('#carusel-zoom .image-zoom').attr('src', $image);}
    $('#carusel-zoom').foundation('open');
    zoomOut();

    e.preventDefault();
  });

  //slider bottom
  var elem = new Foundation.Orbit($('.orbit'), {
    'autoPlay': true,
    'timerDelay': 3000,
    'animInFromLeft': 'fade-in',
    'animInFromRight': 'fade-in',
    'animOutToLeft': 'fade-out',
    'animOutToRight': 'fade-out'
  });

});
