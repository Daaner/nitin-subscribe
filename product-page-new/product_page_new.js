$(document).ready(function() {

  //zoom
  var $panzoom = $('.reveal').find('.panzoom').first();
  var $pancontrol = $('.reveal .control').first();

  function zoomIn() {
    $pancontrol.find('.zoom-in').addClass('disabled');
    $pancontrol.find('.zoom-out').removeClass('disabled');
    $pancontrol.find('.reset').removeClass('disabled');
  }

  function zoomOut() {
    $panzoom.panzoom("setMatrix", [1, 0, 0, 1, 0, -25]);
    $pancontrol.find('.zoom-out').addClass('disabled');
    $pancontrol.find('.reset').addClass('disabled');
    $pancontrol.find('.zoom-in').removeClass('disabled');
  }

  function zoomAll() {
    $pancontrol.find('.zoom-in').removeClass('disabled');
    $pancontrol.find('.zoom-out').removeClass('disabled');
    $pancontrol.find('.reset').removeClass('disabled');
  }

  $panzoom.panzoom({
    minScale: 1,
    maxScale: 2.8,
    rangeStep: 1,
    easing: "ease-in-out",
    $zoomIn: $(".zoom-in"),
    $zoomOut: $(".zoom-out"),
    $reset: $(".reset")
  });

  $panzoom.on('panzoomzoom', function(e, scale, opts, changed) {
    var scaled = $panzoom.panzoom("getMatrix");
    var scale_max = $panzoom.panzoom('option', 'maxScale');
    var scale_min = $panzoom.panzoom('option', 'minScale');
    if (changed) {
      if (opts > 1) {
        if (scaled[0] == scale_max) {
          zoomIn();
        } else {
          zoomAll()
        }
      }
      if (opts < 1) {
        if (scaled[0] == scale_min) {
          zoomOut();
        } else {
          zoomAll()
        }
      }
    }
  });

  $panzoom.on('panzoomreset', function(e, change) {
    if (change) {
      zoomOut();
    }
  });

  $('.orbit-slide a.zoom').on('click', function(e) {
    var $image = $(this).children('img').attr('data-image');
    console.log($image);
    if($image){$('#carusel-zoom .image-zoom').attr('src', $image);}
    $('#carusel-zoom').foundation('open');
    zoomOut();

    e.preventDefault();
  });
});
